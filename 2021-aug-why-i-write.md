Why I write ?

I am strong believer in simplicity and clarity. Simple and clear things and concetpts evolve and survive. Complex things perish. 

A good way to gauge the simplicity of your understanding is to explain it. 
If they understand it quickly, its simple enough. More effort and time, needed for explaining, more complex the thing is. People around you are not interested in your explanations. 

Writing is the closest thing to explaining. With the hope, that someone, some time later, may read and soak it in.
