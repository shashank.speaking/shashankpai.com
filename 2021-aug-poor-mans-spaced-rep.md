# 1. Composition notebook. 
# 2. Deck of index cards.
# 3. Spreadsheet
# 4. Slide deck

All you need to board this cheap train ride.

> I don't need dollar bills to have fun tonight  
> (I love cheap thrills)


**In essence** take notes on new material in the composition book. Group and organize on index cards. Active recall and quiz in excel and powerpoint. 


**Composition book**

One book per subject. Numbered pages. Index on page one.
Take notes purely in linear manner, one page after another. Never have to go back. Add page numbers as references as required.  

Nothing beats the composition book when you put together its ubiquity ( available everywhere ), price ( under a dollar ), quality ( most are good ) and size ( can keep it open between me and the keyboards ). 

**Index cards** 

Review notes from the composition book and group them into index cards. The size limitation helps to keep it sane. Trash cards, reorder them, rewrite them, group them. Size limitation is good, forces splitting information.  

Concept maps fit well into index cards. One card for each parent that has connected children. Stop at level 3.


Nothing is faster for lookups than index cards. Even semi organized information is orders of magnitude faster to find than any other system.  


**Spreadsheet**

One sheet per topic. One column for questions, one for answers per sheet. For answers, make font color = background color. You can only see it in the formula bar, only when the cell is highlighted. Quiz yourself from top to bottom. Anything you can't answer, make it bold.    

**Things I've learned**

Organizing infromation in a hierarchy is a superpower.  
Lay of the land is best represented by a concept map.
