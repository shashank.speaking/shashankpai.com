# Top 5 reasons why your readers will thank you for clickbait titles

Use clickbait for good content too. 


*The unreasonable effectiveness of 3 names* 
Come up with 3 names for the content. On one extreme, something that correctly summarizes the content. On the other, pure clickbait. And one in the middle which is true but borderline clickbait. 