

Tech companies sell hardware and software.

**Horizontal vs vertical**  Apple is vertical, google is horizontal. Vertical = make money off hardware, features usually exclusive to their hardware. Focus is on the experience on the device. Try to maximize features and drive up the price. Not usually a volumes business. e.g Garmin. Horizontal = make money off services, subscriptions, commissions, rent-seeking behaviour. Try to always maximize market share. Try to be available everywhere. Clearly a volumes business. e.g. 

[2014/Jan: Tech business models](https://stratechery.com/2014/business-models-2014/)  


**Google** 

Recommended reading 
[2013/July: Stratechery- Understanding Google](https://stratechery.com/2013/understanding-google/)   
[2014/Jan: Stratechery - Google's business model](https://stratechery.com/2014/googles-new-business-model/)


You can sell a physical [pencil](http://www.fiftythree.com/pencil) for $59.99 with an app included. But not just an app for $4.99. 