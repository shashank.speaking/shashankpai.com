
- [Poor Man's spaced repetition system](2021-aug-poor-mans-spaced-rep.html) Aug/2021
- [DIY Spaced Repetition](https://docs.google.com/document/d/e/2PACX-1vRQRPc_2qcIfQJ1gVLBKtJLktcO8xrlCDnSYFZpdrBomXvqTEppV1ehsPQHkb0BtaKS0QbwnfI9KTt9/pub) Sep/2021

<span> (c) Shashank Pai, 2021 </span>
