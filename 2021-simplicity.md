I see a pattern around me to try and make hard things easier. 

Trying to make harder things easier, has the side effect of making simpler things more complex.

If you have fewer hard and many simpler things, that approach is being penny wise and pound foolish. 

If you have too many hard things, it is wiser to understand the inherent complexity in them. And then break the complexity down. Effectively is making things less harder. 